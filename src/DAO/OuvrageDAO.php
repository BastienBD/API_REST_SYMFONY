<?php

namespace App\DAO;

use App\Entity\Ouvrage;

class OuvrageDAO
{
    function getOuvrages(): array
    {
        global $conn;
        $query = "SELECT O.IdOuvrage, O.TitreVO, O.TitreTraduit, O.AnneePremiereParution, T.NomT, S.NomS
              FROM Ouvrage O
              INNER JOIN Type T ON O.IdType = T.IdType
              INNER JOIN Style S ON O.IdStyle = S.IdStyle;";
        $ouvrages = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newOuvrage = new ouvrage(); // Je crée un objet "ouvrage" vide pour centraliser toutes les données d'un ouvrage
            $newOuvrage->setIdOuvrage($row["IdOuvrage"]); // On remplit cet objet avec les données de la lignes courante de notre SELECT
            $newOuvrage->setTitreVO($row["TitreVO"]);
            $newOuvrage->setTitreTraduit($row["TitreTraduit"]);
            $newOuvrage->setAnneePremiereParution($row["AnneePremiereParution"]);
            $newOuvrage->setIdType($row["NomT"]);
            $newOuvrage->setIdStyle($row["NomS"]);

            $ouvrages[] = $newOuvrage; // Puis on rajoute cet objet au tableau d'ouvrages
        }

        return $ouvrages; // On renvoie la liste des objets ouvrage

    }

    function getOuvrageById($idOuvrage): array
    {
        global $conn;
        $query = "SELECT O.IdOuvrage, O.TitreVO, O.TitreTraduit, O.AnneePremiereParution, T.NomT, S.NomS
              FROM Ouvrage O
              INNER JOIN Type T ON O.IdType = T.IdType
              INNER JOIN Style S ON O.IdStyle = S.IdStyle
              WHERE IdOuvrage = '$idOuvrage';";
        $ouvrage = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newOuvrage = new ouvrage(); // Je crée un objet "ouvrage" vide pour centraliser toutes les données d'un ouvrage
            $newOuvrage->setIdOuvrage($row["IdOuvrage"]); // On remplit cet objet avec les données de la lignes courante de notre SELECT
            $newOuvrage->setTitreVO($row["TitreVO"]);
            $newOuvrage->setTitreTraduit($row["TitreTraduit"]);
            $newOuvrage->setAnneePremiereParution($row["AnneePremiereParution"]);
            $newOuvrage->setType($row["NomT"]);
            $newOuvrage->setStyle($row["NomS"]);

            $ouvrage[] = $newOuvrage; // Puis on rajoute cet objet au tableau d'ouvrages
        }

        return $ouvrage; // On renvoie la liste des objets ouvrage
    }

    function getOuvrageByNameType($NomT): array
    {
        global $conn;
        $query = "SELECT O.IdOuvrage, O.TitreVO, O.TitreTraduit, O.AnneePremiereParution, T.NomT
              FROM Ouvrage O
              INNER JOIN Type T ON O.IdType = T.IdType
              WHERE T.NomT = '$NomT'";
        $ouvrages = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newOuvrage = new ouvrage(); // Je crée un objet "ouvrage" vide pour centraliser toutes les données d'un ouvrage
            $newOuvrage->setIdOuvrage($row["IdOuvrage"]); // On remplit cet objet avec les données de la lignes courante de notre SELECT
            $newOuvrage->setTitreVO($row["TitreVO"]);
            $newOuvrage->setTitreTraduit($row["TitreTraduit"]);
            $newOuvrage->setAnneePremiereParution($row["AnneePremiereParution"]);
            $newOuvrage->setType($row["NomT"]);

            $ouvrages[] = $newOuvrage; // Puis on rajoute cet objet au tableau d'ouvrages
        }

        return $ouvrages; // On renvoie la liste des objets ouvrage
    }

    function getOuvrageByNameStyle($NomS): array
    {
        global $conn;
        $query = "SELECT O.IdOuvrage, O.TitreVO, O.TitreTraduit, O.AnneePremiereParution, S.NomS
              FROM Ouvrage O
              INNER JOIN Style S ON O.IdStyle = S.IdStyle
              WHERE S.NomS = '$NomS'";
        $ouvrages = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newOuvrage = new ouvrage(); // Je crée un objet "ouvrage" vide pour centraliser toutes les données d'un ouvrage
            $newOuvrage->setIdOuvrage($row["IdOuvrage"]); // On remplit cet objet avec les données de la lignes courante de notre SELECT
            $newOuvrage->setTitreVO($row["TitreVO"]);
            $newOuvrage->setTitreTraduit($row["TitreTraduit"]);
            $newOuvrage->setAnneePremiereParution($row["AnneePremiereParution"]);
            $newOuvrage->setStyle($row["NomS"]);

            $ouvrages[] = $newOuvrage; // Puis on rajoute cet objet au tableau d'ouvrages
        }

        return $ouvrages; // On renvoie la liste des objets ouvrage
    }
}
