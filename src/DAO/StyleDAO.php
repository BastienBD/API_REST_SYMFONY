<?php

namespace App\DAO;

use App\Entity\Style;

class StyleDAO
{
    function getStyles(): array
    {
        global $conn;
        $query = "SELECT *
                  FROM Style;";
        $styles = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newStyle = new style();
            $newStyle->setIdStyle($row["IdStyle"]);
            $newStyle->setNomS($row["NomS"]);
            $newStyle->setDescriptifS($row["DescriptifS"]);

            $styles[] = $newStyle;
        }

        return $styles;
    }

    function getStyleByIdStyle($IdStyle): array
    {
        global $conn;
        $query = "SELECT *
                  FROM Style
                  WHERE IdStyle = '$IdStyle';";
        $style = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newStyle = new style();
            $newStyle->setIdStyle($row["idStyle"]);
            $newStyle->setNomS($row["NomS"]);
            $newStyle->setDescriptifS($row["DescriptifS"]);

            $style[] = $newStyle;
        }

        return $style;
    }

    function getStyleByName($NomS): array
    {
        global $conn;
        $query = "SELECT *
                  FROM Style
                  WHERE NomT = '$NomS';";
        $styles = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newStyle = new style();
            $newStyle->setIdStyle($row["IdStyle"]);
            $newStyle->setNomS($row["NomS"]);
            $newStyle->setDescriptifS($row["DescriptifS"]);

            $styles[] = $newStyle;
        }

        return $styles;
    }
}