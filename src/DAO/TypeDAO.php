<?php

namespace App\DAO;

use App\Entity\Type;

class TypeDAO
{
    function getTypes() : array
    {
        global $conn;
        $query = "SELECT *
                  FROM Type;";
        $types = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newType = new type();
            $newType->setIdType($row["IdType"]);
            $newType->setNomT($row["NomT"]);
            $newType->setDescriptifT($row["DescriptifT"]);

            $types[] = $newType;
        }

        return $types;
    }

    function getTypeByIdType($IdType): array
    {
        global $conn;
        $query = "SELECT *
                  FROM Type
                  WHERE IdType = '$IdType';";
        $type = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newType = new type();
            $newType->setIdType($row["IdType"]);
            $newType->setNomT($row["NomT"]);
            $newType->setDescriptifT($row["DescriptifT"]);

            $type[] = $newType;
        }

        return $type;
    }

    function getTypeByName($NomT): array
    {
        global $conn;
        $query = "SELECT *
                  FROM Type
                  WHERE NomT = '$NomT';";
        $types = [];
        $result = mysqli_query($conn, $query);

        while ($row = mysqli_fetch_assoc($result)) {
            $newType = new type();
            $newType->setIdType($row["IdType"]);
            $newType->setNomT($row["NomT"]);
            $newType->setDescriptifT($row["DescriptifT"]);

            $types[] = $newType;
        }

        return $types;
    }
}