<?php

namespace App\Entity;

use App\Repository\TypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TypeRepository::class)]
class Type
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $IdType;

    #[ORM\Column(type: 'string', length: 20)]
    private $NomT;

    #[ORM\Column(type: 'string', length: 200)]
    private $DescriptifT;

    #[ORM\OneToMany(mappedBy: 'IdType', targetEntity: Ouvrage::class)]
    private $ouvrages;

    public function __construct()
    {
        $this->ouvrages = new ArrayCollection();
    }

    public function getIdType(): ?int
    {
        return $this->IdType;
    }

    public function setIdType(int $IdType): self
    {
        $this->IdType = $IdType;

        return $this;
    }

    public function getNomT(): ?string
    {
        return $this->NomT;
    }

    public function setNomT(string $NomT): self
    {
        $this->NomT = $NomT;

        return $this;
    }

    public function getDescriptifT(): ?string
    {
        return $this->DescriptifT;
    }

    public function setDescriptifT(string $DescriptifT): self
    {
        $this->DescriptifT = $DescriptifT;

        return $this;
    }

    /**
     * @return Collection<int, Ouvrage>
     */
    public function getOuvrages(): Collection
    {
        return $this->ouvrages;
    }

    public function addOuvrage(Ouvrage $ouvrage): self
    {
        if (!$this->ouvrages->contains($ouvrage)) {
            $this->ouvrages[] = $ouvrage;
            $ouvrage->setIdType($this);
        }

        return $this;
    }

    public function removeOuvrage(Ouvrage $ouvrage): self
    {
        if ($this->ouvrages->removeElement($ouvrage)) {
            // set the owning side to null (unless already changed)
            if ($ouvrage->getIdType() === $this) {
                $ouvrage->setIdType(null);
            }
        }

        return $this;
    }
}
