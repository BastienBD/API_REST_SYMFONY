<?php

namespace App\Entity;

use App\Repository\StyleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StyleRepository::class)]
class Style
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $IdStyle;

    #[ORM\Column(type: 'string', length: 20)]
    private $NomS;

    #[ORM\Column(type: 'string', length: 200)]
    private $DescriptifS;

    #[ORM\OneToMany(mappedBy: 'IdStyle', targetEntity: Ouvrage::class)]
    private $ouvrages;

    public function __construct()
    {
        $this->ouvrages = new ArrayCollection();
    }

    public function getIdStyle(): ?int
    {
        return $this->IdStyle;
    }

    public function setIdStyle(int $IdStyle): self
    {
        $this->IdStyle = $IdStyle;

        return $this;
    }

    public function getNomS(): ?string
    {
        return $this->NomS;
    }

    public function setNomS(string $NomS): self
    {
        $this->NomS = $NomS;

        return $this;
    }

    public function getDescriptifS(): ?string
    {
        return $this->DescriptifS;
    }

    public function setDescriptifS(string $DescriptifS): self
    {
        $this->DescriptifS = $DescriptifS;

        return $this;
    }

    public function removeOuvrage(Ouvrage $ouvrage): self
    {
        if ($this->ouvrages->removeElement($ouvrage)) {
            // set the owning side to null (unless already changed)
            if ($ouvrage->getIdStyle() === $this) {
                $ouvrage->setIdStyle(null);
            }
        }

        return $this;
    }
}
