<?php

namespace App\Entity;

use App\Repository\OuvrageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OuvrageRepository::class)]
class Ouvrage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $IdOuvrage;

    #[ORM\Column(type: 'string', length: 200)]
    private $TitreVO;

    #[ORM\Column(type: 'string', length: 200)]
    private $TitreTraduit;

    #[ORM\Column(type: 'integer')]
    private $AnneePremiereParution;

    #[ORM\ManyToOne(targetEntity: Type::class, inversedBy: 'ouvrages')]
    #[ORM\JoinColumn(nullable: false)]
    private $IdType;

    #[ORM\ManyToOne(targetEntity: Style::class, inversedBy: 'ouvrages')]
    #[ORM\JoinColumn(nullable: false)]
    private $IdStyle;

    public function getIdOuvrage(): ?int
    {
        return $this->IdOuvrage;
    }

    public function setIdOuvrage(int $IdOuvrage): self
    {
        $this->IdOuvrage = $IdOuvrage;

        return $this;
    }

    public function getTitreVO(): ?string
    {
        return $this->TitreVO;
    }

    public function setTitreVO(string $TitreVO): self
    {
        $this->TitreVO = $TitreVO;

        return $this;
    }

    public function getTitreTraduit(): ?string
    {
        return $this->TitreTraduit;
    }

    public function setTitreTraduit(string $TitreTraduit): self
    {
        $this->TitreTraduit = $TitreTraduit;

        return $this;
    }

    public function getAnneePremiereParution(): ?int
    {
        return $this->AnneePremiereParution;
    }

    public function setAnneePremiereParution(int $AnneePremiereParution): self
    {
        $this->AnneePremiereParution = $AnneePremiereParution;

        return $this;
    }

    public function getIdType(): ?Type
    {
        return $this->IdType;
    }

    public function setIdType(?Type $IdType): self
    {
        $this->IdType = $IdType;

        return $this;
    }

    public function getIdStyle(): ?Style
    {
        return $this->IdStyle;
    }

    public function setIdStyle(?Style $IdStyle): self
    {
        $this->IdStyle = $IdStyle;

        return $this;
    }
}
